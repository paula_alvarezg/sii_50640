#include "../include/DatosMemoriaCompartida.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>


int main(){

	DatosMemCompartida* pdatos;
	int fd_datos;
	void* proy;

	fd_datos=open("/tmp/Datos.txt",O_RDWR);
	proy=mmap(NULL,sizeof(*pdatos), PROT_READ|PROT_WRITE, MAP_SHARED, fd_datos, 0);
	close(fd_datos);
	pdatos=(DatosMemCompartida*)proy;

	while(1){
		usleep(25000);
		if((pdatos->esfera.centro.y)>(pdatos->raqueta1.y2)&&(pdatos->esfera.centro.x)<=0)
			(pdatos->accion)=1;
		else if((pdatos->esfera.centro.y)<(pdatos->raqueta1.y1)&&(pdatos->esfera.centro.x)<=0)
			(pdatos->accion)=-1;
		else
			(pdatos->accion)=0;
	}
}
