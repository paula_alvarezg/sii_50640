#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>

int main (){

	char cad[150];
	mkfifo("/tmp/tuberia",0777);
	int fd=open("/tmp/tuberia",O_RDONLY);

	while(read(fd,cad,sizeof(cad))){
		printf("%s\n",cad);
	}
	close(fd);
	unlink("/tmp/tuberia");

	return 0;
}
