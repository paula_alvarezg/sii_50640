// Mundo.cpp: implementation of the CMundo class.
//
//Modificado por Paula Álvarez Gutiérrez
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <error.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{/*
	//for(int i=0;i<esferas.size();i++)                      
		delete esferas[i]; 
	*/munmap(pdatos,sizeof(datos));                                     
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	/*for(int i=0;i<esferas.size();i++)                                                       
		esferas[i]->Dibuja();     */                                          

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);

	cuenta++;
	if(esfera.getRadio()>=0.15){
		if(cuenta%320==0)
			esfera.radio=(esfera.getRadio()-0.05);
	}


	/*if(cuenta%703==0)                                            
	{
		Esfera* e=new Esfera();

		esferas.push_back(e);
	}   */                                                                                 

	/*for(int i=0;i<esferas.size();i++)                                                    
		esferas[i]->Mueve(0.025f);     */                                                
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
		/*for(int j=0;j<esferas.size();j++)                                    
			paredes[i].Rebota(*(esferas[j]));              */                     
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	/*for(int i=0;i<esferas.size();i++) {
		jugador1.Rebota(*esferas[i]);
		jugador2.Rebota(*esferas[i]);
	}*/
	
	char cad[150];

	if(fondo_izq.Rebota(esfera))
	{
		esfera.radio=0.5;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.radio=0.5;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}


	/*for(int i=0;i<esferas.size();i++){ 
		if(fondo_izq.Rebota(*esferas[i])){
			esferas[i]->radio=0.5;
			esferas[i]->centro.x=0;
			esferas[i]->centro.y=rand()/(float)RAND_MAX;
			esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
			esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
		}

		if(fondo_dcho.Rebota(*esferas[i])){
			esferas[i]->radio=0.5;
			esferas[i]->centro.x=0;
			esferas[i]->centro.y=rand()/(float)RAND_MAX;
			esferas[i]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esferas[i]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;
		}
	}*/

//BOT//
	
	pdatos->raqueta1=jugador1;
	pdatos->esfera=esfera;
	if(pdatos->accion==1)             //ir hacia arriba
		OnKeyboardDown('w',0,0);
	else if(pdatos->accion==-1)       //ir hacia abajo
		OnKeyboardDown('s',0,0);
	else                            //no se mueve
		jugador1.velocidad.y=0;

//recepción de coordenadas//
	char cad_coord[200];
	s_comunic.Receive(cad_coord,sizeof(cad_coord));
	sscanf(cad_coord,"%f %f %f %f %f %f %f %f %f %f %d %d",&esfera.centro.x, &esfera.centro.y,&jugador1.x1,&jugador1.y1,&jugador1.x2, 			&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad_t[5];
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}
	sprintf(cad_t,"%c",key);
	s_comunic.Send(cad_t,sizeof(cad_t));
}

void CMundo::Init()
{

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

//bot//
	if((fd_datos=open("/tmp/Datos.txt",O_RDWR|O_CREAT|O_TRUNC,0777))<0)
		perror("error al abrir el fichero");
	write(fd_datos,&datos,sizeof(datos));
	void *proy;
	proy=mmap(NULL, sizeof(datos), PROT_READ|PROT_WRITE, MAP_SHARED, fd_datos, 0);
	close(fd_datos);
	pdatos=(DatosMemCompartida*)proy;

	pdatos->accion=0;
	
	
	printf("Introduzca su nombre:");
	gets(nombre);
	s_comunic.Connect((char*)"127.0.0.1",4800);
	s_comunic.Send(nombre, sizeof(nombre));


}



